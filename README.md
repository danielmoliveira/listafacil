Projeto referente a disciplina de android da FA7.

ListaFacil é um app para organizar sua lista de compras.
- Cadastrar produtos, valores e quantidade.
- Cálculo de total por produto e total geral.
- Produto pode ser editado com apenas um clique.
- Produto pode ser excluído com um clique longo.