package br.edu.fa7.listafacil;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
/**
 * Trabalho da Disciplina de Android FA7
 * 
 * @author danieloliveira
 * @since 2014-03-31
 * 
 */
public class SplashScreen extends Activity{
	
	private final int SPLASH_DISPLAY_LENGHT = 2000;
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.splash_screen);
		getActionBar().hide();
		
		new Handler().postDelayed(new Runnable() {
			
			@Override
			public void run() {
				Intent i = new Intent(SplashScreen.this, MainActivity.class);
				SplashScreen.this.startActivity(i);
				SplashScreen.this.finish();
			}
		}, SPLASH_DISPLAY_LENGHT);
	}

}
