package br.edu.fa7.listafacil;

import java.io.ByteArrayOutputStream;
import java.io.FileNotFoundException;
import java.io.InputStream;

import android.app.Activity;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.Bundle;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.Toast;
/**
 * Trabalho da Disciplina de Android FA7
 * 
 * @author danieloliveira
 * @since 2014-03-31
 * 
 */
public class FormularioProduto extends Activity implements OnClickListener {
	
	private Button btnSave;
	private Button btnCancel;
	private EditText edtName;
	private EditText edtPreco;
	private EditText edtQtd;
	private ImageView imgProduto;
	private Bitmap bitmap;
	private long id = -1;
	byte[] buffer = null;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);
		setContentView(R.layout.formulario_produto);
		
		btnSave = (Button) findViewById(R.id.BtnOk);
		btnCancel = (Button) findViewById(R.id.BtnCancel);
		
		btnSave.setOnClickListener(this);
		btnCancel.setOnClickListener(this);
		
		edtName = (EditText) findViewById(R.id.formName);
		edtPreco = (EditText) findViewById(R.id.formPreco);
		edtQtd = (EditText) findViewById(R.id.formQtd);
		imgProduto = (ImageView) findViewById(R.id.imgProduto);
		imgProduto.setOnClickListener(this);
		
		/*
		 * Recebe o objeto enviado de outra Activity
		 */
		Produto p = (Produto) getIntent().getSerializableExtra("produto");
		if (p != null) {
			edtName.setText(p.getNome());
			String preco = String.format("%.2f", p.getPreco());
			edtPreco.setText(preco);
			String quantidade = String.valueOf(p.getQuantidade());
			edtQtd.setText(quantidade);
			if (p.getBufferImg() != null)
				imgProduto.setImageBitmap(BitmapFactory.decodeByteArray(p.getBufferImg(), 0, p.getBufferImg().length));
			id = p.getId();
		}
	}

	@Override
	public void onClick(View v) {
		Intent it;
		switch (v.getId()) {
		case R.id.BtnOk:
			if (camposPreenchidos()) {
				it = new Intent(FormularioProduto.this, MainActivity.class);
				Produto produto = new Produto();
				
				//Recuperando informações digitadas na tela
				produto.setNome(edtName.getText().toString());
				double preco = Double.parseDouble(edtPreco.getText().toString().replace(",", "."));
				produto.setPreco(preco);
				int quantidade = Integer.parseInt(edtQtd.getText().toString());
				produto.setQuantidade(quantidade);
				
				if (bitmap != null) {
					produto.setBufferImg(buffer);
				}
				
				//Setando o id que veio no objeto de outra Activity para edição
				if (id != -1) {
					produto.setId(id);
				}
				
				it.putExtra("produto", produto);
				
				setResult(RESULT_OK, it);
				finish();
			}
			break;

		case R.id.BtnCancel:
			it = new Intent(FormularioProduto.this, MainActivity.class);
			setResult(RESULT_CANCELED, it);
			finish();
			break;
			
		case R.id.imgProduto:
			it = new Intent();
			
			//Localiza imagem do Celular
			it.setType("image/*");
			it.setAction(Intent.ACTION_GET_CONTENT);
			it.addCategory(Intent.CATEGORY_OPENABLE);
			
			startActivityForResult(it, 2);
			break;
		}
		
	}
	
	@Override
	protected void onActivityResult(int requestCode, int resultCode, Intent data) {
		InputStream stream = null;
		if (requestCode == 2 && resultCode == RESULT_OK) {
			if (bitmap != null) {
				bitmap.recycle();
			}
			try {
				//Transforma os dados recebidos da Intent em Array de bytes
				stream = getContentResolver().openInputStream(data.getData());
				//converte array de bytes em um Bitmap
				bitmap = BitmapFactory.decodeStream(stream);
				
				imgProduto.setImageBitmap(bitmap);
			} catch (FileNotFoundException e) {
				e.printStackTrace();
			}
			
		}
	}
	
	/**
	 * Valida se os campos foram preenchidos
	 * @return true, false
	 */
	private boolean camposPreenchidos() {
		if("".equals(edtName.getText().toString())) {
			msgError("Nome do Produto");
			return false;
		}
		if("".equals(edtPreco.getText().toString())) {
			msgError("Preço");
			return false;
		}
		if("".equals(edtQtd.getText().toString())) {
			msgError("Quantidade");
			return false;
		}
		if (bitmap != null) {
			buffer = getImageForId(bitmap);
			//Se a imagem for muito grande, a mesma nao poderá ser salva 
			if (buffer.length > 200000) {
				msgError("com uma Imagem menor!");
				return false;
			}
		}
		
		return true;
	}
	
	/**
	 * Método que notifica o usuário através de mensagens na tela
	 * @param message
	 */
	public void msgError(String campo) {
		Toast.makeText(this, "Favor preencha o campo " + campo, Toast.LENGTH_SHORT).show();
	}
	
	/**
	 * Método que recebe um Bitmap 
	 * converte para PNG e retorna bytes
	 * @param Bitmap
	 * @return byte[]
	 */
	private byte[] getImageForId(Bitmap bitmap) {
		ByteArrayOutputStream baos = new ByteArrayOutputStream();
		bitmap.compress(Bitmap.CompressFormat.PNG, 100, baos);

		return baos.toByteArray();
	}
}
