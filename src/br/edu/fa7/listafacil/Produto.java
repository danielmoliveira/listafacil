package br.edu.fa7.listafacil;

import java.io.Serializable;
/**
 * Trabalho da Disciplina de Android FA7
 * 
 * @author danieloliveira
 * @since 2014-03-31
 * 
 */
public class Produto implements Serializable{

	/**
	 * 
	 */
	private static final long serialVersionUID = 5867544897204646115L;
	
	private long id;
	private String nome;
	private double preco;
	private int quantidade;
	private byte[] bufferImg;

	public Produto(String nome, double preco, int quantidade, byte[] img) {
		this.nome = nome;
		this.preco = preco;
		this.quantidade = quantidade;
		this.bufferImg = img;
	}

	public Produto() {
		// TODO Auto-generated constructor stub
	}

	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}

	public String getNome() {
		return nome;
	}

	public void setNome(String nome) {
		this.nome = nome;
	}

	public double getPreco() {
		return preco;
	}

	public void setPreco(double preco) {
		this.preco = preco;
	}

	public int getQuantidade() {
		return quantidade;
	}

	public void setQuantidade(int quantidade) {
		this.quantidade = quantidade;
	}

	public byte[] getBufferImg() {
		return bufferImg;
	}

	public void setBufferImg(byte[] bufferImg) {
		this.bufferImg = bufferImg;
	}

}
