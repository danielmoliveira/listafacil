package br.edu.fa7.listafacil;

import java.io.ByteArrayOutputStream;
import java.util.ArrayList;
import java.util.List;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.AdapterView.OnItemLongClickListener;
import android.widget.Button;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

/**
 * Trabalho da Disciplina de Android FA7
 * 
 * Classe que exibe um ListView preenchido com todos os produtos cadastrados no
 * banco
 * 
 * @author danieloliveira
 * @since 2014-03-31
 * 
 */
public class MainActivity extends Activity implements OnItemClickListener,
		OnItemLongClickListener, OnClickListener {

	private ProdutoAdapter adapter;
	private ListView listView;
	private List<Produto> produtos;
	private Button btnAdd;
	private TextView tvSubTotal;
	private ProdutoDAO dao;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_main);

		dao = new ProdutoDAO(this);

		listView = (ListView) findViewById(R.id.listView);
		listView.setOnItemClickListener(this);
		listView.setOnItemLongClickListener(this);

		btnAdd = (Button) findViewById(R.id.BtnAdd);
		btnAdd.setOnClickListener(this);

		tvSubTotal = (TextView) findViewById(R.id.tvSubTotal);

		carregaListaProdutos();

	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {

		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.main, menu);
		return true;
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		// Handle action bar item clicks here. The action bar will
		// automatically handle clicks on the Home/Up button, so long
		// as you specify a parent activity in AndroidManifest.xml.
		int id = item.getItemId();
		if (id == R.id.action_settings) {
			return true;
		}
		return super.onOptionsItemSelected(item);
	}

	@Override
	public boolean onItemLongClick(AdapterView<?> parent, View view,
			int position, long id) {

		final int posicao = position;
		AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(this);

		// set title
		alertDialogBuilder.setTitle("Deseja excluir?");

		// set dialog message
		alertDialogBuilder
				.setMessage("Clique em Sim para excluir!")
				.setCancelable(false)
				.setPositiveButton("Sim",
						new DialogInterface.OnClickListener() {
							public void onClick(DialogInterface dialog, int id) {
								Produto produto = produtos.get(posicao);
								produtos.remove(produto);
								dao.excluir(produto);
								carregaListaProdutos();
								setMessage("excluído");
							}
						})
				.setNegativeButton("Não",
						new DialogInterface.OnClickListener() {
							public void onClick(DialogInterface dialog, int id) {
								// if this button is clicked, just close
								// the dialog box and do nothing
								dialog.cancel();
							}
						});

		// create alert dialog
		AlertDialog alertDialog = alertDialogBuilder.create();

		// show it
		alertDialog.show();
		return true;
	}

	@Override
	public void onItemClick(AdapterView<?> parent, View view, int position,
			long id) {

		Produto produto = produtos.get(position);
		Intent i = new Intent(MainActivity.this, FormularioProduto.class);

		i.putExtra("produto", produto);
		startActivityForResult(i, 1);

	}

	@Override
	public void onClick(View v) {
		Intent i;
		switch (v.getId()) {
		case R.id.BtnAdd:
			i = new Intent(MainActivity.this, FormularioProduto.class);
			startActivityForResult(i, 0);
			break;

		default:
			break;
		}

	}

	@Override
	protected void onActivityResult(int requestCode, int resultCode, Intent data) {
		if (resultCode == RESULT_OK) {

			Produto p = (Produto) data.getSerializableExtra("produto");

			if (requestCode == 0) {
				// Caso a imagem não seja preenchida, insere a default
				if (p.getBufferImg() == null) {
					p.setBufferImg(getImageForId(R.drawable.ic_no_image));
				}
				// Adiciona o produto a lista e ao Banco
				produtos.add(p);
				dao.inserir(p);

				setMessage("cadastrado");
			} else if (requestCode == 1) {
				// Caso a imagem seja nula, continua a imagem antiga.
				if (p.getBufferImg() == null) {
					Produto pd = dao.buscarProduto((int) p.getId());
					p.setBufferImg(pd.getBufferImg());
				}
				// Atualiza o produto no banco
				dao.atualizar(p);

				setMessage("atualizado");
			}
			carregaListaProdutos();
		}
	}

	/**
	 * Método que busca todos os registros no banco, insere em uma lista e
	 * recarrega o ListView com o adapter
	 */
	private void carregaListaProdutos() {
		produtos = new ArrayList();
		produtos = dao.buscarTodosProdutos();

		adapter = new ProdutoAdapter(this, produtos);
		listView.setAdapter(adapter);

		calculaSubTotal();
	}

	/**
	 * Método que calcula e informa o subtotal na tela
	 */
	private void calculaSubTotal() {
		double subTotal = 0;
		for (int i = 0; i < produtos.size(); i++) {
			subTotal += produtos.get(i).getPreco()
					* produtos.get(i).getQuantidade();
		}
		tvSubTotal.setText("Subtotal: R$ " + String.format("%.2f", subTotal));
	}

	/**
	 * Método que recebe um ID de um Drawable e retorna bytes
	 * 
	 * @param id
	 * @return bytes de um drawable
	 */
	private byte[] getImageForId(int id) {
		Bitmap bitmap = BitmapFactory.decodeResource(getResources(), id);
		ByteArrayOutputStream baos = new ByteArrayOutputStream();
		bitmap.compress(Bitmap.CompressFormat.PNG, 100, baos);

		return baos.toByteArray();
	}

	/**
	 * Método que notifica o usuário através de mensagens na tela
	 * 
	 * @param message
	 */
	private void setMessage(String message) {
		Toast.makeText(this, "Produto " + message + " com sucesso!",
				Toast.LENGTH_SHORT).show();
	}

}
