package br.edu.fa7.listafacil;

import java.util.ArrayList;
import java.util.List;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
/**
 * Trabalho da Disciplina de Android FA7
 * 
 * @author danieloliveira
 * @since 2014-03-31
 * 
 */
public class ProdutoDAO {
	
	private final static String TABLE_NAME = "produtos";
	private final static String DATABASE_NAME = "ListaFacil.db";
	private final static String CREATE_TABLE = "CREATE TABLE " + TABLE_NAME +" ("
			+ "_id INTEGER PRIMARY KEY AUTOINCREMENT, "
			+ "nome TEXT NOT NULL, " 
			+ "preco DOUBLE NOT NULL, "
			+ "quantidade INTEGER NOT NULL, "
			+ "logotipo BLOB)";
	private final static int DATABASE_VERSION = 1;
	
	private SQLHelper sqlHelper;
	private SQLiteDatabase db;
	
	public ProdutoDAO(Context context) {
		sqlHelper = new SQLHelper(context, DATABASE_NAME, DATABASE_VERSION, CREATE_TABLE, TABLE_NAME);
		db = sqlHelper.getWritableDatabase();
	}
	
	/*
	 * Cadastra um novo produto
	 */
	public void inserir(Produto produto) {
		ContentValues values = getProduto(produto);

		db.insert(TABLE_NAME, null, values);
	}

	
	/*
	 * Atualiza um produto
	 */
	public void atualizar(Produto produto) {
		ContentValues values = getProduto(produto);

		String[] id = { String.valueOf(produto.getId()) };
		db.update(TABLE_NAME, values, "_id = ?", id);
	}
	
	/*
	 * Exclui um produto
	 */
	public void excluir(Produto produto) {
		String[] id = { String.valueOf(produto.getId()) };
		db.delete(TABLE_NAME, "_id = ?", id);
	}
	
	/*
	 * Localiza um produto pelo seu id
	 */
	public Produto buscarProduto(int id) {
		String[] columns = { "_id", "nome", "preco", "quantidade", "logotipo" };
		String[] param = { String.valueOf(id) };
		Cursor c = db.query(TABLE_NAME, columns, "_id = ?", param, null, null, "_id");
		c.moveToFirst();
		
		Produto p = setProduto(c);
		
		return p;
	}
	
	/*
	 * Lista todos os produtos ordenados pelo Nome
	 */
	public List<Produto> buscarTodosProdutos() {

		List<Produto> lista = null;
		String[] columns = { "_id", "nome", "preco", "quantidade", "logotipo" };
		Cursor c = db.query(TABLE_NAME, columns, null, null, null, null, "nome");
		
		lista = new ArrayList<Produto>();
		if (c.getCount() > 0) {
			c.moveToFirst();
			while (!c.isAfterLast()) {
			
				Produto p = setProduto(c);
				
				lista.add(p);
				c.moveToNext();
			}
		}
		
		return lista;
	}

	private Produto setProduto(Cursor c) {
		Produto p = new Produto();
		p.setId(c.getInt(0));
		p.setNome(c.getString(1));
		p.setPreco(c.getDouble(2));
		p.setQuantidade(c.getInt(3));
		p.setBufferImg(c.getBlob(4));
		return p;
	}
	
	private ContentValues getProduto(Produto produto) {
		ContentValues values = new ContentValues();
		values.put("nome", produto.getNome());
		values.put("preco", produto.getPreco());
		values.put("quantidade", produto.getQuantidade());
		values.put("logotipo", produto.getBufferImg());
		return values;
	}

}
