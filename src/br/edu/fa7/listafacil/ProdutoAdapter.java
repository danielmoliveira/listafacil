package br.edu.fa7.listafacil;

import java.util.List;

import android.content.Context;
import android.graphics.BitmapFactory;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;
/**
 * Trabalho da Disciplina de Android FA7
 * 
 * @author danieloliveira
 * @since 2014-03-31
 * 
 */
public class ProdutoAdapter extends BaseAdapter{
	
	private Context context;
	private List<Produto> produtos;

	public ProdutoAdapter(Context context, List<Produto> produtos) {
		this.context = context;
		this.produtos = produtos;
	}

	@Override
	public int getCount() {
		return this.produtos.size();
	}

	@Override
	public Object getItem(int pos) {
		return this.produtos.get(pos);
	}

	@Override
	public long getItemId(int position) {
		return this.produtos.get(position).getId();
	}

	@Override
	public View getView(int position, View convertView, ViewGroup parent) {

		Produto produto = this.produtos.get(position);

		LayoutInflater inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);

		View v = inflater.inflate(R.layout.list_products, null);
		
		ImageView img = (ImageView) v.findViewById(R.id.imgItem);
		
		//Seta o icone da app como imagem do produto
//		img.setImageResource(R.drawable.ic_launcher);
		
		/*Caso exista alguma imagem cadastrada, entao a mesma
		 * será exibida no ListView
		 */
		if (produto.getBufferImg() != null) 
			img.setImageBitmap(BitmapFactory.decodeByteArray(produto.getBufferImg(), 0, produto.getBufferImg().length));
		
		TextView nomeProduto = (TextView) v.findViewById(R.id.txtNome);
		nomeProduto.setText(produto.getNome());
		
		TextView preco = (TextView) v.findViewById(R.id.txtPreco);
		//Tratamento para exibicao em forma de moeda
		String valor = String.format("%.2f", produto.getPreco());
		preco.setText("R$ " + valor);
		
		TextView qtd = (TextView) v.findViewById(R.id.txtQtd);
		String quantidade = String.valueOf(produto.getQuantidade());
		qtd.setText("Qtd: " + quantidade);
		
		TextView total = (TextView) v.findViewById(R.id.txtTotal);
		//Valor total é a qtd * o valor
		String valorTotal = String.format("%.2f", produto.getQuantidade() * produto.getPreco());
		total.setText("R$ " + valorTotal);
		

		return v;
	}

}
